class Guess:
    def __init__(self, truth, sortedPredictions):
        self.truth = truth
        self.predictions = sortedPredictions #[label, score]

    def wasCorrect(self, allowedErrors=1):
        return self.truth in [pred[0] for pred in self.predictions[:allowedErrors]]