import math
def Euclidian(probe, reference):
    return math.sqrt(sum(pow(a-b,2) for a, b in zip(probe, reference)))

def Euclidian_Median(probe, reference):
    return Euclidian(probe.sample, reference.median)

def Euclidian_Mean(probe, reference):
    return Euclidian(probe.sample, reference.mean)

def Manhatten(probe, refernece):
    return sum(abs(a-b) for a, b in zip(probe,refernece))

def Manhatten_Median(probe, reference):
    return Manhatten(probe.sample, reference.median)

def Manhatten_Mean(probe, reference):
    return Manhatten(probe.sample, reference.mean)

def KeyVote(probe, reference):
    return list([abs(p1-r1) for p1, r1 in zip(probe.sample, reference.mean)])

#Sorts scores in ascending order
def CompareDistance(predictions):
    test = list(sorted(predictions, key=lambda x:x[1],reverse=False))
    return test

def KeyVote_Comperator(predictions):
    ranks_for_each_letter=list()
    for letterIndex in range(len(predictions[0][1])):
        valuesForLetter=[(l[0], l[1][letterIndex]) for l in predictions]
        ranks_for_letter = CompareDistance(valuesForLetter)
        ranks_for_each_letter.append([x[0] for x in ranks_for_letter])

    distance_per_label=list()
    labels=[pred[0] for pred in predictions]
    for label in labels:
        totalRank=0
        for letterRank in ranks_for_each_letter:
            totalRank+=letterRank.index(label)
        distance_per_label.append((label, totalRank))

    distance_per_label = CompareDistance(distance_per_label)
    return distance_per_label

def WeightedEuclidian(probe, reference):
    return math.sqrt(sum(pow((a-b)*(1-c),2) for a, b, c in zip(probe.sample, reference.mean, reference.std)))

def ScaledMannhatten_Mean(probe, reference):
    return sum(abs(a-b)/(1-c) for a, b, c in zip(probe.sample, reference.mean, reference.std))

def ScaledMannhatten_Median(probe, reference):
    return sum(abs(b-a)/(1-c) for a, b, c in zip(probe.sample, reference.median, reference.std))

def dist3_mean(probe, reference):
    stdDivs = [abs(b - a) / (1-c) for a, b, c in zip(probe.sample, reference.mean, reference.std)]
    score=0
    for div in stdDivs:
        if div > sum(reference.std)/len(reference.std):
            score += 1
    return score

ScoreSystems = {
    'euclidian_{median}': Euclidian_Median,
    'euclidian_{mean}': Euclidian_Mean,
    'manhatten_{median}': Manhatten_Median,
    'manhatten_{mean}': Manhatten_Mean,
    'keyvote': KeyVote,
    'weighted euclidian': WeightedEuclidian,
    'scaled manhatten_{mean}': ScaledMannhatten_Mean,
    'scaled manhatten_{median}': ScaledMannhatten_Median,
    'dist3_{mean}': dist3_mean
}

Comperators = {
    'euclidian_{median}': CompareDistance,
    'euclidian_{mean}': CompareDistance,
    'manhatten_{median}': CompareDistance,
    'manhatten_{mean}': CompareDistance,
    'keyvote': KeyVote_Comperator,
    'weighted euclidian': CompareDistance,
    'scaled manhatten_{mean}': CompareDistance,
    'scaled manhatten_{median}': CompareDistance,
    'dist3_{mean}': CompareDistance
}