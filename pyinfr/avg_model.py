import numpy as np

class avg_model:
    def __init__(self, label):
        self.label=label



class wmodel(avg_model):
    def __init__(self, label, entries):
        transposed=[list(i) for i in zip(*entries)]
        self.label = label
        self.mean=[np.mean(transposed[x]) for x in range(len(transposed))]
        self.median=[np.median(transposed[x]) for x in range(len(transposed))]
        self.std=[np.std(transposed[x]) for x in range(len(transposed))]

class imodel(avg_model):
    def __init__(self, a, b, entries):
        self.a = a
        self.b = b

        self.label=f"{a}->{b}"
        self.mean=list([np.mean(entries)])
        self.median=list([np.median(entries)])
        self.std=list([np.std(entries)])
