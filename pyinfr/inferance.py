import pickle
import os

import numpy as np

from iutils import window
from TimeModes import timing_methods
from SimpleProbe import Probe
from avg_model import wmodel, imodel
from ScoringSystems import ScoreSystems, Comperators
from Guess import Guess
labels=list()
probes=list()
references=dict()

def load_data():
    with open('labels.lst', 'rb') as labelFile:
        labels_=pickle.load(labelFile)
        for x in labels_:
            labels.append(x)

    with open('probes.lst', 'rb') as probeFile:
        probes_=pickle.load(probeFile)
        for x in probes_:
            probes.append(list((x[0], x[1], *x[2])))

    for label in os.listdir('references'):
        label = label[:len(labels[0])]
        with open(f"references/{label}.lst", 'rb') as refFile:
            references_ = pickle.load(refFile)
            references[label]=list()
            for ref in references_:
                references[label].append(list((ref[0], ref[1], *ref[2])))
            ##references[label]=pickle.load(refFile)


def normalize_time():
    for probe in probes:
        last=probe[-1]
        probe[2:] = [x/last for x in probe[2:]]
    for refs in references.values():
        for ref in refs:
            last=ref[-1]
            ref[2:] = [x/last for x in ref[2:]]





#Load the data
load_data()
#Normalize the time in probes and references
normalize_time()
#print(len(references['which']))
#print(probes[0])
#print(len(list(filter(lambda x: x[0] == 0, probes))))

for timeMethod in timing_methods:
    method = timing_methods[timeMethod]
    refModels = list()
    for label in references:
        referenceTimings = [method(entry) for entry in references[label]]
        refModels.append(wmodel(labels.index(label), referenceTimings))

    timedProbes = list([Probe(x[0], x[1], method(x)) for x in probes])

    #Evaluate all probes
    for scoringSystem in ScoreSystems:
        score_method = ScoreSystems[scoringSystem]
        gusses=list()
        for probe in timedProbes:
            predictions = list()
            for reference in refModels:
                predictions.append((reference.label, score_method(probe, reference)))
            gusses.append(Guess(probe.truth, Comperators[scoringSystem](predictions)))
        #print(f"timing:{timeMethod}, scoreing: {scoringSystem}:")
        #print(f"\\begin{{lstlisting}}[caption=Success rate after 1-10 guesses, for {scoringSystem} when using {timeMethod},frame=tlrb]{{Name}}")
        print(
        f"\t\\multirow{{10}}{{*}}{{ \\textbf{{ ${timeMethod}$ }} }}&\\multirow{{10}}{{*}}{{ \\textbf{{ ${scoringSystem}$ }} }}")

        for i in range(1,11):
            wrong=0
            correct=0
            wrong_per_label=dict()
            correct_per_label=dict()

            for label in labels:
                wrong_per_label[labels.index(label)] = 0
                correct_per_label[labels.index(label)] = 0

            for guess in gusses:
                if guess.wasCorrect(allowedErrors=i):
                    correct_per_label[guess.truth] += 1
                    correct += 1
                else:
                    wrong += 1
                    wrong_per_label[guess.truth] += 1
            if i == 1:
                print(f"\t\t\t&{i} & {correct} & {wrong} & {(int)((correct) / (correct + wrong) * 100)}\\% \\\\ \\cline{{3-6}}")
            elif i == 10:
                print(f"\t\t\t&&{i} & {correct} & {wrong} & {(int)((correct)/(correct+wrong)*100)}\\% \\\\ \\cline{{1-6}}")
            else:
                print(f"\t\t\t&&{i} & {correct} & {wrong} & {(int)((correct)/(correct+wrong)*100)}\\% \\\\ \\cline{{3-6}}")
            ##print(f"\t[Gusses: {i}]:{correct} correct, {wrong} wrong, {(correct)/(correct+wrong)*100}% successrate")
        #print("\end{lstlisting}")
        #print("\hfill")
      #  score_per_label = list([(labels[lbl], correct_per_label[lbl]) for lbl in correct_per_label.keys()])
      #  score_per_label = list(sorted(score_per_label, key=lambda x: x[1], reverse=True))
      ##  print(score_per_label)
        #for label in labels:
        #    print(f"\t{label}: {correct_per_label[labels.index(label)]}/{wrong_per_label[labels.index(label)]}")

print(f"Number of words: {len(labels)}")

Refs = dict()
IProbes = list()
def setup_interkey():
    iReference = dict()
    for refKey in references.keys():
        entries = references[refKey]
        entries = list([list(window(x[2:], 2)) for x in entries])
        i=0
        for kp in window(refKey, 2):
            latencies=[x[i][1] - x[i][0] for x in entries]
            i += 1
            if kp not in iReference.keys():
                iReference[kp] = list()
            iReference[kp].extend(latencies)

    for refKey in iReference.keys():
        Refs[f"{refKey[0]}->{refKey[1]}"] = imodel(refKey[0], refKey[1], iReference[refKey])
        ##Refs.append(imodel(refKey[0], refKey[1], iReference[refKey]))


    for probe in probes:
        word_label = labels[probe[0]]
        entries = list(window(probe[2:], 2))
        i=0
        for kp in window(word_label, 2):
            IProbes.append(Probe(f"{kp[0]}->{kp[1]}", probe[1], list([entries[i][0]-entries[i][0]])))
            ##IProbes.append(list([f"{kp[0]}->{kp[1]}", probe[1], entries[i][1]-entries[i][0]]))
            i+=1



def test_interkey():
    for scoreSystem in ScoreSystems:
        print("Testing key to key, with score: ", scoreSystem)
        mated_scores = list()
        nonmated_scores = list()
        method=ScoreSystems[scoreSystem]
        for probe in IProbes:
            ref = Refs[probe.truth]
            mated_scores.append(method(probe, ref))
            for ref_key in Refs.keys():
                if ref_key != probe.truth:
                    ref = Refs[ref_key]
                    nonmated_scores.append(method(probe, ref))

        with open(f"{scoreSystem}_mated.txt", 'w') as f:
            for mated in mated_scores:
                f.write(f"{mated}\n")

        with open(f"{scoreSystem}_nonmated.txt", 'w') as f:
            for nonmated in nonmated_scores:
                f.write(f"{nonmated}\n")

        print("\tMean_mated: " , np.mean(mated_scores))
        print("\tMean_nonmated:", np.mean(nonmated_scores))
#setup_interkey()
#test_interkey()
