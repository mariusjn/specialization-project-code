def abs_timing(timings):
    return timings[3:-1]

def inter_key(timings):
    inter_key_delays=list()
    for i in range(len(timings) - 3):
        inter_key_delays.append(timings[i+3] - timings[i+2])
    return inter_key_delays
    ##return list(timings[0], timings[1], *inter_key_delays)

timing_methods={
    'absolute_timing': abs_timing,
    'interkey_timing': inter_key
}