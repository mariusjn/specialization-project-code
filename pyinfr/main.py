import os
import csv
import random
import numpy as np
import pickle
from itertools import groupby

ROOT_PATH = "/home/frostbyte/specialization/ktscra/data-sample-100"


class WordEntry:
    def __init__(self, pID, wordID, entries):
        self.participant = int(pID)
        self.id = wordID
        self.entries = entries

    def get_normalized_time(self):
        last = self.entries[-1]
        return [entry / last for entry in self.entries[1:-1]]

    def get_inter_key_time(self):
        inter_key_delays = list()
        time = self.entries
        for i in range(len(time) - 1):
            inter_key_delays.append(time[i + 1] - time[i])
        return inter_key_delays

    def get_normalized_inter_key_delay(self):
        inter_key_delays = list()
        last = self.entries[-1]

        time = [entry / last for entry in self.entries]

        # time = self.entries
        for i in range(len(time) - 1):
            inter_key_delays.append(time[i + 1] - time[i])

        # inter_key_delays = [ww / last for ww in inter_key_delays]
        return inter_key_delays


words = list()


def get_word_id(word):
    if word not in words:
        words.append(word)
    print(f"{word} -> {words.index(word)}")
    return words.index(word)
    '''
    
    if word not in words.keys():
        words[word] = len(words.keys())
    return words[word]
    '''


def load_word(word: str):
    wordID = get_word_id(word)
    word_entries = list()
    with open(f"{ROOT_PATH}/{len(word)}/{word}.csv") as csvFile:
        wordEntryReader = csv.reader(csvFile)
        next(wordEntryReader)  # Skip header
        for row in wordEntryReader:
            word_entries.append(WordEntry(row[0], wordID, [int(entry) for entry in row[1:]]))

    return word_entries


def generate_fingerprint1(entries: list):
    transposed = [list(i) for i in zip(*entries)]
    answer = list()
    for i in range(len(transposed)):
        answer.append((np.mean(transposed[i]), np.std(transposed[i])))
    return answer


def split_dataset(probeCount=20):
    for f in os.listdir('references'):
        os.remove(f"references/{f}")
    word_length = 7
    probes = list()
    for x in os.listdir(f"{ROOT_PATH}/{word_length}"):
        word = x[:word_length]
        entries = load_word(word)
        wordid = get_word_id(word)

        entries = list(sorted(entries, key=lambda w: w.participant))
        participantsEntries = dict()

        for entry in entries:
            if entry.participant not in participantsEntries.keys():
                participantsEntries[entry.participant] = list()
            participantsEntries[entry.participant].append(entry.entries)

        #numEntries = sum([len(participantsEntries[ww]) for ww in list(participantsEntries.keys())[:probeCount]])
        #print(f"{word} has {numEntries} probe entries")

        for probeid in list(participantsEntries.keys())[:probeCount]:
            for entry in participantsEntries[probeid]:
                probe = (wordid, probeid, entry)
                probes.append(probe)

        references = list()
        for referenceid in list(participantsEntries.keys())[probeCount:]:
            for entry in participantsEntries[referenceid]:
                reference = (wordid, referenceid, entry)
                references.append(reference)

        with open(f"references/{word}.lst", 'wb') as refFile:
            pickle.dump(references, refFile)

        numParticipants = len(participantsEntries.keys())
        print(f"{numParticipants} participants typed the word {word}")

    with open("probes.lst", 'wb') as probefile:
        pickle.dump(probes, probefile)

    with open("labels.lst", 'wb') as labelFile:
        pickle.dump(words, labelFile)


split_dataset()
