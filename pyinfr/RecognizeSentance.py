import csv
import pickle
import os

import numpy as np

from iutils import window
from itertools import groupby
from TimeModes import timing_methods
from SimpleProbe import Probe
from avg_model import wmodel, imodel
from ScoringSystems import ScoreSystems, Comperators
from Guess import Guess

ROOT_PATH = "/home/frostbyte/specialization/ktscra"
SEN_FILE = "WouldSuggestYouAndMarkAddressThisTogether2.csv"

words = list()


def get_word_id(word):
    if word not in words:
        words.append(word)
    ##print(f"{word} -> {words.index(word)}")
    return words.index(word)


def load_word(word: str):
    wordID = get_word_id(word)
    word_entries = list()
    with open(f"{ROOT_PATH}/data-sample-100/{len(word)}/{word}.csv") as csvFile:
        wordEntryReader = csv.reader(csvFile)
        next(wordEntryReader)  # Skip header
        for row in wordEntryReader:
            word_entries.append(WordEntry(row[0], wordID, [int(entry) for entry in row[1:]]))

    return word_entries


class WordEntry:
    def __init__(self, pID, wordID, entries):
        self.participant = int(pID)
        self.id = wordID
        self.entries = entries

    def get_normalized_time(self):
        last = self.entries[-1]
        return [entry / last for entry in self.entries[1:-1]]

    def get_inter_key_time(self):
        inter_key_delays = list()
        time = self.entries
        for i in range(len(time) - 1):
            inter_key_delays.append(time[i + 1] - time[i])
        return inter_key_delays

    def get_normalized_inter_key_delay(self):
        inter_key_delays = list()
        last = self.entries[-1]

        time = [entry / last for entry in self.entries]

        # time = self.entries
        for i in range(len(time) - 1):
            inter_key_delays.append(time[i + 1] - time[i])

        # inter_key_delays = [ww / last for ww in inter_key_delays]
        return inter_key_delays


wordLengths = list()
word_loads = dict()


def load_all_words():
    for f in os.listdir('references'):
        os.remove(f"references/{f}")

    for word_length in wordLengths:
        words_for_length = list()
        for x in os.listdir(f"{ROOT_PATH}/data-sample-100/{word_length}"):
            word = x[:word_length]
            wordid = get_word_id(word)
            entries = load_word(word)
            entries = list(sorted(entries, key=lambda w: w.participant))
            participantsEntries = dict()

            for entry in entries:
                if entry.participant not in participantsEntries.keys():
                    participantsEntries[entry.participant] = list()
                participantsEntries[entry.participant].append(entry.entries)
            words_for_length.append((wordid, word, participantsEntries))

        word_loads[word_length] = words_for_length





class Sentance:
    def __init__(self, id, words):
        self.participant = id
        split_indicies = [idx for idx, val in enumerate(words) if val == -1]
        res = [words[i + 1: j] for i, j in zip(([-1] + split_indicies), (split_indicies + [len(words)]))]
        self.words = res



COMPLETE_SENTANCE_STR=list()
def load_sentance():
    sentance_entries = list()
    with open(f"{ROOT_PATH}/{SEN_FILE}") as csvFile:
        wordEntryReader = csv.reader(csvFile)
        HEADR = next(wordEntryReader)[1:]
        HEADR = list(HEADR)
        print(HEADR)
        split_indicies = [idx for idx, val in enumerate(HEADR) if val == ' ']
        res = [''.join(HEADR[i + 1: j]) for i, j in zip(([-1] + split_indicies), (split_indicies + [len(HEADR)]))]
        for x in res:
            COMPLETE_SENTANCE_STR.append(x)
            if len(x) not in wordLengths and len(x) > 2:
                wordLengths.append(len(x))
        for row in wordEntryReader:
            print(row)
            sentance_entries.append(Sentance(row[0], [int(entry) for entry in row[1:]]))
    return sentance_entries


def setup_reference(probe_participants):
    references_for_length = dict()
    for word_length in list(word_loads.keys()):
        references_for_length[word_length] = list()
        for tup in list(word_loads[word_length]):
            wordid = tup[0]
            word = tup[1]
            participantEntries = tup[2]
            references = list()
            for participant in list(participantEntries.keys()):
                if participant not in probe_participants:
                    for entry in participantEntries[participant]:
                        reference = [wordid, participant, *entry]
                        references.append(reference)
            with open(f"references/{word}.lst", 'wb') as refFile:
                pickle.dump(references, refFile)
            references_for_length[word_length].append(references)
    return references_for_length


probe_sentances = load_sentance()

# Generate all words
load_all_words()

blacklisted_participants = [x.participant for x in probe_sentances]
references_by_length = setup_reference(blacklisted_participants)


def normalize_time():
    for probe_sen in probe_sentances:
        for word_probe in probe_sen.words:
            last = word_probe[-1]
            word_probe[1:] = [x / last for x in word_probe[1:]]
            word_probe[0:] = [-404, int(probe_sen.participant)] + word_probe

        print(probe_sen.words)
    for word_length in list(references_by_length.keys()):
        to_keep22 = list()
        for references in list(references_by_length[word_length]):
            to_keep = list()
            for reference in references:
                if len(reference) == word_length + 2:
                    last = reference[-1]
                    reference[2:] = [x / last for x in reference[2:]]
                    to_keep.append(reference)
                else:
                    print("WIERRD")
                    print(reference)
            to_keep22.append(to_keep)
        references_by_length[word_length] = to_keep22

normalize_time()
print("Normalization done, prepared data")
print("Example normalized data: probe then a reference")
#print(probe_sentances[0].words[4])
#print(references_by_length[4][0][0])


def convert_to_dict():
    for word_length in references_by_length.keys():
        normal_refs = references_by_length[word_length]
        word_refs = dict()
        for references in normal_refs:
            word_idx = references[0][0]
            word_refs[words[word_idx]] = references
        references_by_length[word_length] = word_refs
        '''
        print(normal_refs[0])
        print("ww")
        sorted_by_word = sorted(normal_refs, key=lambda x: x[0])
        grouped_by_word = (groupby(sorted_by_word, key=lambda x: x[0]))
        for key, values in grouped_by_word:
            print(key)
            print("ww")
            word_refs[key] = list(values)
        print(word_refs)
'''

convert_to_dict()

def do_general_methods():
    for timeMethod in timing_methods:
        method = timing_methods[timeMethod]
        refModels_by_length = dict()
        for word_length in references_by_length.keys():
            refModels = list()
            references = references_by_length[word_length]
            for label in references:
                referenceTimings = [method(entry) for entry in references[label]]
                refModels.append(wmodel(get_word_id(label), referenceTimings))
            refModels_by_length[word_length] = refModels

        #Now start with the probes
        timed_sentances = list()
        for sentance in probe_sentances:
            new_sen = Sentance(sentance.participant, [0, 0, 0])
            new_sen.words = list([Probe(x[0], x[1], method(x)) for x in sentance.words])
            timed_sentances.append(new_sen)

        Infer_Sentance(refModels_by_length, timed_sentances[0].words, timeMethod)

def printcellcolor(guess, truth):
    if guess == truth:
       # print(f"{guess} == {truth}")
        return f"\\cellcolor{{correct}}{{{guess}}}"
    else:
       # print(f"{guess} != {truth}")
        return guess


def Infer_Sentance(refs_by_length, probes_in_sentance, timeMethod):
    for scoringSystem in ScoreSystems:
        score_method = ScoreSystems[scoringSystem]
        ##words_in_sentance = ["would", "suggest", "you", "and", "mark", "address", "this", "together"]
        words_in_sentance = COMPLETE_SENTANCE_STR

        i = 0
        guesses = list()

        ##print(words_in_sentance)
        for probe in probes_in_sentance:
            probe.truth = get_word_id(words_in_sentance[i])
            probe_length = len(words_in_sentance[i])
            i += 1
            if probe_length > 2:
                refModels = refs_by_length[probe_length]
                predictions = list()
                for reference in refModels:
                    predictions.append((reference.label, score_method(probe, reference)))
                guesses.append(Guess(probe.truth, Comperators[scoringSystem](predictions)))


        #print(f"{timeMethod}::{scoringSystem} ->")
        rows=[[words[guess.truth], *[words[x[0]] for x in guess.predictions[:5]]] for guess in guesses]

        #print(rows)
        rows=np.transpose(rows)
        #print(rows)

        print("""\\begin{subfigure}[b]{0.3\\textwidth}
                \centering
                \makebox[\linewidth]{
\\resizebox{1\linewidth}{!}{%""")
        print("\\begin{tabular}{|l|c|c|c|}\n\\hline")
        print("\\textbf{Truth} & \\textbf{can} & \\textbf{return} & \\textbf{early}\\\\ \hline")
        print("\\multirow{5}{*}{\\textbf{Predictions}}")

        for row in rows[1:]:
            print(f"&{printcellcolor(row[0], rows[0][0])} & {printcellcolor(row[1], rows[0][1])} & {printcellcolor(row[2], rows[0][2])}\\\\ \cline{{2-4}}")
        print("\\end{tabular}")
        print(f"}}}}\n\caption{{Sentance prediction, timing: {timeMethod}, scoremetric:{scoringSystem}}}")
        print(f"\\label{{fig:sentance{timeMethod}{scoringSystem}}}")
        print("""\\end{subfigure}""")

        print("\\hfill")

        #for guess in guesses:
        #    print(f"\t\t{words[guess.truth]}: {guess.wasCorrect(allowedErrors=5)}, {[words[x[0]] for x in guess.predictions[:5]]}")
            #print(f"\t\t{words[guess.truth]}: {guess.wasCorrect(allowedErrors=5)}, {[[words[x[0]], (1+guess.predictions[5][1])/(1+x[1]) * 100] for x in guess.predictions[:5]]}")
        #print(words[guesses[0].truth])
        #print(guesses[0].truth)
        #print(guesses[0].predictions)

do_general_methods()

'''

setup_reference(blacklisted_participants)
def load_data():
    with open('labels.lst', 'rb') as labelFile:
        labels_=pickle.load(labelFile)
        for x in labels_:
            labels.append(x)

    with open('probes.lst', 'rb') as probeFile:
        probes_=pickle.load(probeFile)
        for x in probes_:
            probes.append(list((x[0], x[1], *x[2])))

    for label in os.listdir('references'):
        label = label[:len(labels[0])]
        with open(f"references/{label}.lst", 'rb') as refFile:
            references_ = pickle.load(refFile)
            references[label]=list()
            for ref in references_:
                references[label].append(list((ref[0], ref[1], *ref[2])))
            ##references[label]=pickle.load(refFile)


def normalize_time():
    for probe in probes:
        last=probe[-1]
        probe[2:] = [x/last for x in probe[2:]]
    for refs in references.values():
        for ref in refs:
            last=ref[-1]
            ref[2:] = [x/last for x in ref[2:]]
'''

# Load the data
# load_data()
# Normalize the time in probes and references
# normalize_time()
# print(len(references['which']))
# print(probes[0])
# print(len(list(filter(lambda x: x[0] == 0, probes))))

'''
for timeMethod in timing_methods:
    method = timing_methods[timeMethod]
    refModels = list()
    for label in references:
        referenceTimings = [method(entry) for entry in references[label]]
        refModels.append(wmodel(labels.index(label), referenceTimings))

    timedProbes = list([Probe(x[0], x[1], method(x)) for x in probes])

    #Evaluate all probes
    for scoringSystem in ScoreSystems:
        score_method = ScoreSystems[scoringSystem]
        gusses=list()
        for probe in timedProbes:
            predictions = list()
            for reference in refModels:
                predictions.append((reference.label, score_method(probe, reference)))
            gusses.append(Guess(probe.truth, Comperators[scoringSystem](predictions)))
        print(f"timing:{timeMethod}, scoreing: {scoringSystem}:")
        for i in range(1,11):
            wrong=0
            correct=0
            wrong_per_label=dict()
            correct_per_label=dict()

            for label in labels:
                wrong_per_label[labels.index(label)] = 0
                correct_per_label[labels.index(label)] = 0

            for guess in gusses:
                if guess.wasCorrect(allowedErrors=i):
                    correct_per_label[guess.truth] += 1
                    correct += 1
                else:
                    wrong += 1
                    wrong_per_label[guess.truth] += 1

            print(f"\t[{i}]:{correct} correct, {wrong} wrong, {(correct)/(correct+wrong)*100}% successrate")

      #  score_per_label = list([(labels[lbl], correct_per_label[lbl]) for lbl in correct_per_label.keys()])
      #  score_per_label = list(sorted(score_per_label, key=lambda x: x[1], reverse=True))
      ##  print(score_per_label)
        #for label in labels:
        #    print(f"\t{label}: {correct_per_label[labels.index(label)]}/{wrong_per_label[labels.index(label)]}")

print(f"Number of words: {len(labels)}")

Refs = dict()
IProbes = list()
def setup_interkey():
    iReference = dict()
    for refKey in references.keys():
        entries = references[refKey]
        entries = list([list(window(x[2:], 2)) for x in entries])
        i=0
        for kp in window(refKey, 2):
            latencies=[x[i][1] - x[i][0] for x in entries]
            i += 1
            if kp not in iReference.keys():
                iReference[kp] = list()
            iReference[kp].extend(latencies)

    for refKey in iReference.keys():
        Refs[f"{refKey[0]}->{refKey[1]}"] = imodel(refKey[0], refKey[1], iReference[refKey])
        ##Refs.append(imodel(refKey[0], refKey[1], iReference[refKey]))


    for probe in probes:
        word_label = labels[probe[0]]
        entries = list(window(probe[2:], 2))
        i=0
        for kp in window(word_label, 2):
            IProbes.append(Probe(f"{kp[0]}->{kp[1]}", probe[1], list([entries[i][0]-entries[i][0]])))
            ##IProbes.append(list([f"{kp[0]}->{kp[1]}", probe[1], entries[i][1]-entries[i][0]]))
            i+=1



def test_interkey():
    for scoreSystem in ScoreSystems:
        print("Testing key to key, with score: ", scoreSystem)
        mated_scores = list()
        nonmated_scores = list()
        method=ScoreSystems[scoreSystem]
        for probe in IProbes:
            ref = Refs[probe.truth]
            mated_scores.append(method(probe, ref))
            for ref_key in Refs.keys():
                if ref_key != probe.truth:
                    ref = Refs[ref_key]
                    nonmated_scores.append(method(probe, ref))

        with open(f"{scoreSystem}_mated.txt", 'w') as f:
            for mated in mated_scores:
                f.write(f"{mated}\n")

        with open(f"{scoreSystem}_nonmated.txt", 'w') as f:
            for nonmated in nonmated_scores:
                f.write(f"{nonmated}\n")

        print("\tMean_mated: " , np.mean(mated_scores))
        print("\tMean_nonmated:", np.mean(nonmated_scores))
#setup_interkey()
#test_interkey()
'''
