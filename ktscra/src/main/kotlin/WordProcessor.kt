import dataset.ALL_SENTANCES
import dataset.Keystroke
import dataset.TypeSession

typealias KeySequence = List<Keystroke>
data class WordEntry (
    val participant: Int,
    val keystrokes: KeySequence
    )

data class Word (
    val stringified: String,
    val entries: List<WordEntry>
    )

val INVALID_TIME = Keystroke(-1, -1, -1, "<ERROR>", -1)

suspend fun ExtractWords(session: TypeSession, includeMistakes: Boolean = false, lowercase: Boolean = true): List<KeySequence> {
    val sentance = ALL_SENTANCES[session.sentance]

    var keystrokes = mutableListOf<Keystroke>()
    //println(session.entries.joinToString(","){it.INPUT})

    //var currentTyped = ""
    for (entry in session.entries) {
        //Skip shifts for now
        if (entry.INPUT == "SHIFT" || entry.INPUT == "CAPS_LOCK" || entry.INPUT == "CTRL") continue

        //todo: figureout if insert is an error
        if (entry.INPUT == "INSERT") continue

        if (entry.INPUT == "BKSP" || entry.INPUT.contains("ARW") || entry.INPUT.contains("DELETE")) {
            var lastNonError = keystrokes.indexOfLast { it != INVALID_TIME }
            if (lastNonError != -1)
                keystrokes.removeAt(lastNonError)
            //keystrokes.removeLast() //Remove 1


            keystrokes.add(INVALID_TIME)
        } else {
            keystrokes.add(entry)
            //currentTyped += entry.INPUT

        }
    }
    var words = mutableListOf<List<Keystroke>>()
    var currentWord = mutableListOf<Keystroke>()
    for (keystroke in keystrokes) {
        if (keystroke.INPUT == " ") { //New word
            words.add(currentWord)
            currentWord = mutableListOf()
        } else if (keystroke.INPUT != "." && keystroke.INPUT != "," && keystroke.INPUT != "?" && keystroke.INPUT != ";"){
            if (lowercase)
                keystroke.INPUT = keystroke.INPUT.lowercase()
            currentWord += keystroke
        }
    }
    words.add(currentWord)

    if (!includeMistakes)
        words.removeIf { it.any { it == INVALID_TIME } }

    return words
}

suspend fun ProcessWord(id: Int, typingSessions: List<TypeSession>, correctMistakes: Boolean = false): List<Word> {
    return typingSessions.flatMap { ExtractWords(it, correctMistakes) }.groupBy { it.joinToString(""){it.INPUT} }.map {
        Word(it.key, it.value.map { WordEntry(id, it) })
    }
}