import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import dataset.*
import kotlinx.coroutines.*
import java.io.File
import kotlin.system.measureTimeMillis

val threadpool = newFixedThreadPoolContext(48, "Threadpool-1")

val sanitizationBlacklist = arrayOf(
    3, //Data corrupt in my copy
    24101, 252492,
    //Extremely large times
    73518, 114073, 164812, 257486, 318660

)

fun DumpCSV(word: Word, path: String = "") {
    var file = File("$path/${word.stringified.length}")
    file.mkdirs()
    file = File("${file.absolutePath}/${word.stringified}.csv")
    csvWriter().open(file) {
        val header = word.stringified.toCharArray().map { it.toString() }.toMutableList()
        header.add(0, "ID")
        writeRow(header)
        word.entries.take(1000).forEach {
            val firstPresstime = it.keystrokes.first().PRESS_TIME
            val entry = it.keystrokes.map { it.PRESS_TIME - firstPresstime }.toMutableList()
            entry.add(0, it.participant.toLong())
            writeRow(entry)
        }
    }
}

fun main(args: Array<String>) = runBlocking {
    val dataReader = ZippedDataReader(args[0])
    var participants = dataReader.getParticipants()

    participants = participants.filter { it.PARTICIPANT_ID !in sanitizationBlacklist }

    csvWriter().open("participants.csv") {
        //writeRow(participants.map { it.PARTICIPANT_ID })

        writeRow(participants.map { it.AVG_WPM_15 })
    }

    print("${participants.count()} participants in dataset, of which ")
    participants = participants.filter { candidate ->
        candidate.PARTICIPANT_ID !in sanitizationBlacklist &&
                candidate.NATIVE_LANGUAGE == "en" &&
                candidate.KEYBOARD_TYPE == KeyboardType.full &&
                //         candidate.FINGERS in TypeStyle.Medium .. TypeStyle.`10+` &&
                (candidate.FINGERS == TypeStyle.Medium || candidate.FINGERS == TypeStyle.Proficent || candidate.FINGERS == TypeStyle.Good)
        //        candidate.FINGERS == TypeStyle.`9-10`


        candidate.TIME_SPENT_TYPING > 3

                && candidate.ERROR_RATE <= 0.2f
                && candidate.AVG_WPM_15 > 60
        //&& candidate.HAS_TAKEN_TYPING_COURSE
    }
    println("${participants.count()} participants meet the filter criteria")
    //Mask i can load with current ram settings is 120_000 participants it seems, 110_000 is easy but start getting alcoation issues after that
    participants = participants.take(110_000)
    //val benchmark = measureTimeMillis {

    //Load all the participants matching our criteria's session data
    var sessionsWithParticipants = participants.map {
        async(threadpool) { Pair(it, dataReader.getParticipantData(it.PARTICIPANT_ID)) }
    }.awaitAll().filter { it.second.isNotEmpty() }.toMap()

    println("Successfully loaded ${sessionsWithParticipants.size} participants session data, totalling ${sessionsWithParticipants.values.sumOf { it.size }} typing sessions and ${sessionsWithParticipants.values.sumOf { it.sumOf { it.entries.size } }} keystrokes")
    //}

    File("data-sample-100").deleteRecursively()
/*
    val benchmark = measureTimeMillis {
        val w = sessionsWithParticipants.flatMap { ProcessWord(it.key.PARTICIPANT_ID, it.value) }
        var words = w.groupBy { it.stringified }.map { Word(it.key, it.value.flatMap { it.entries }) }.filter { it.stringified.length > 2 && it.stringified.isNotBlank() }

        //fixme: NOTE, ONLY ALLOW ALPHANUMERIC WORDS
        words = words.filter { !it.stringified.contains(Regex("[^A-Za-z0-9]")) }

        words.groupBy { it.stringified.length }.toSortedMap().forEach { i, words ->
            println("Word with $i letters (${words.count { it.entries.size > 100 }} out of ${words.count()})")
            words.filter { it.entries.count() > 100 }.sortedByDescending { it.entries.count() }.take(100).forEach {
                println("\t${it.stringified}(${it.entries.count()})")
                DumpCSV(it, "data-sample-100")
            }
        }
    }
    println("Took us $benchmark ms")

*/
    val benchmark22 = measureTimeMillis {
        val w = sessionsWithParticipants.flatMap { ProcessWord(it.key.PARTICIPANT_ID, it.value) }
        var words = w.groupBy { it.stringified }.map { Word(it.key, it.value.flatMap { it.entries }) }
            .filter { it.stringified.length > 2 && it.stringified.isNotBlank() }

        //fixme: NOTE, ONLY ALLOW ALPHANUMERIC WORDS
        words = words.filter { !it.stringified.contains(Regex("[^A-Za-z0-9]")) }

        val wordsThatWereExported = mutableListOf<Word>()
        words.groupBy { it.stringified.length }.toSortedMap().forEach { i, words ->
            println("Word with $i letters (${words.count { it.entries.size > 100 }} out of ${words.count()})")
            words.filter { it.entries.count() > 100 }.sortedByDescending { it.entries.count() }.take(50).forEach {
                println("\t${it.stringified}(${it.entries.count()})")
                DumpCSV(it, "data-sample-100")
                wordsThatWereExported.add(it)
            }
        }
        val oooo = File("WouldSuggestYouAndMarkAddressThisTogether2.csv")
        val wouldSuggest = mutableListOf<Pair<Int, List<Keystroke>>>()
        val wwww = wordsThatWereExported.flatMap { it.entries }.groupBy { it.participant }.forEach {
            val wordspes =
                it.value.map { Pair(it.keystrokes.joinToString("") { it.INPUT }, it.keystrokes) }.groupBy { it.first }
            val wordsTypes = wordspes
                .map { it.key }

            val parID = it.key
            val sessions = sessionsWithParticipants.entries.first { it.key.PARTICIPANT_ID == parID }.value
            val sentances = sessions.map { it.sentance }
            val reconstructSentances = sessions.map {
                Pair(
                    it.entries.joinToString("") { it.INPUT }.replace("SHIFT", "").replace(",", "").replace(".", "")
                        .toLowerCase(), it
                )
            }
                .filter { !it.first.contains("BKSP") && !it.first.contains("CAPS_LOCK") && !it.first.contains("CTRL") }

            reconstructSentances.map { Pair(it.first.split(" "), it.second) }
                .sortedBy { it.first.count { wordsTypes.contains(it) } }.forEach {
                    val count = it.first.count { wordsTypes.contains(it) }
                    val x = it.first.joinToString(" ") { it }
                    val chance = count.toFloat() / x.split(" ").size
                    if (chance > 0.5)
                        println("$x: ${chance * 100}%")
                    if (x == "i can return earlier") {

                    //if (x == "they play atlanta here saturday and sunday afternoonsww") {
                        var wwwwww = it.second.entries.indexOfLast { it.INPUT == "." }
                        if (wwwwww == -1)
                            wwwwww = it.second.entries.size
                        var relatedEntries = it.second.entries.subList(it.second.entries.indexOfFirst { it.INPUT != " " && it.INPUT != "SHIFT" && it.INPUT != "CTRL" }, wwwwww)
                        if (relatedEntries.first().INPUT == " ")
                            relatedEntries = relatedEntries.subList(1, relatedEntries.size)

                        relatedEntries = relatedEntries.filter { it.INPUT != "SHIFT" }

                        wouldSuggest.add(Pair(it.second.participant, relatedEntries))

                        println(relatedEntries.joinToString(" ") { it.INPUT })

                        /*
                            println(it.second.entries.joinToString(""){it.INPUT})

                            println(it.first.joinToString(" ") { it } + ": " + count)
                            println("\t${it.first.filter { wordsTypes.contains(it) }}")
                            */
                    }
                }
        }

        csvWriter().open(oooo) {
            val x = wouldSuggest.first().second.joinToString("") { it.INPUT }.map { it.toString() }.toMutableList()
            x.add(0, "ID")
            writeRow(x)
            wouldSuggest.forEach {
                var y = it.second.map { if (it.INPUT == " ") -1 else it.PRESS_TIME }
                /*
                var y = it.second.windowed(3) {
                    when {
                        it[0].INPUT == " " -> {
                            -1
                        } //Middle of two words

                        else -> it[0].PRESS_TIME
                    }
                }
                */

                var wwww = mutableListOf<Long>()
                var startNum = -1L
                y.forEachIndexed { index, l ->
                    if (l == -1L) {
                        startNum = l
                        wwww.add(l)
                    }

                    else if (startNum == -1L) {
                        startNum = l
                        wwww.add(0)
                    } else {
                        wwww.add(l - startNum)
                    }
                }


                println(y)
                println(wwww)
                wwww.add(0, it.first.toLong())

                writeRow(wwww)
            }
        }

        //println(wordsTypes)
        //println(it.value.first().keystrokes.joinToString())


        /*
        val
                wwww =
        sessionsWithParticipants.filter { par -> words.any { it.entries.count { it.participant == par.key.PARTICIPANT_ID } > 0 } }.mapValues {
            it.value.map { ExtractWords(it) }
        }
        val first = wwww.entries.first()
        println(first.key.PARTICIPANT_ID)
        println("Types: ${first.value.size} sentances, ")
        first.value.forEachIndexed { index, lists ->
            println("[$index] ${lists.size}, ${lists.joinToString(" ") {it.joinToString("") { it.INPUT }}}")

        }


         */
    }
    println("Took us $benchmark22 ms")

    val interKeyBenchmark = measureTimeMillis {
        val w = sessionsWithParticipants.flatMap { ProcessWord(it.key.PARTICIPANT_ID, it.value) }
        var words = w.groupBy { it.stringified }.map { Word(it.key, it.value.flatMap { it.entries }) }
            .filter { it.stringified.length > 2 && it.stringified.isNotBlank() }
        words = words.filter { !it.stringified.contains(Regex("[^A-Za-z0-9]")) }

        var GoBetween = words.flatMap { CreateInterKeyValue(it) }
    }


    /*
    println(x.size)
    println(words.size)
    println(x.sumOf { it.entries.size })
    println(words.sumOf { it.entries.size })
    */


    //val participantWithError = sessionsWithParticipants.entries.filter {it.value.any { it.entries.count { it.INPUT == "BKSP" } > 2} }.first()
    //val x = ProcessWord(participantWithError.key.PARTICIPANT_ID, participantWithError.value.filter { it.entries.count { it.INPUT == "BKSP" } > 2})

    /*
    val participantWords = sessionsWithParticipants.map {
        async(threadpool) { Pair(it.key, ProcessWord(it.key.PARTICIPANT_ID, it.value)) }
    }.awaitAll().toMap()
    */


    /*

    //FIXME: NOTE HERE IS REMOVALC ODE OF SESSIONS WITH MISTAKES AKA BKSP
    val mistakes = sessionsWithParticipants.values.sumOf { it.count { it.entries.any { it.INPUT == "BKSP" } } }
    sessionsWithParticipants = sessionsWithParticipants.map { Pair(it.key, it.value.filter { !it.entries.any {it.INPUT == "BKSP"} }) }.toMap()
    println("Mistakes where corrected: " + mistakes)
    println("Cleaned \t\t\t${sessionsWithParticipants.size} participants session data, totalling ${sessionsWithParticipants.values.sumOf { it.size }} typing sessions and ${sessionsWithParticipants.values.sumOf { it.sumOf { it.entries.size } }} keystrokes")


    //println("That took $benchmark miliseconds, or ${benchmark / 1000} seconds or ${benchmark / 60_000} minutes")
    var popSentances = sessionsWithParticipants.values.flatMap { it }.groupBy { it.sentance }.map { Pair(it.key, it.value.distinctBy { it.participant }.count()) }.sortedByDescending { it.second }



    var mostCommonWords = popSentances.map { Pair(ALL_SENTANCES[it.first].split(" "), it.second) }
    .flatMap {
        it.first.map { word -> Pair(word, it.second) }
    }
        //Make similar words the same, aka remove uppercases and remove endings
        //.map { Pair(it.first.lowercase().removeSuffix(".").removeSuffix(","), it.second) }

          //Remove words that have endings or capital start
        .filter { !it.first.isEmpty() }
            //FIXME: CODE FOR FILTERING THE CRITERIA OF WORDS
        .map { Pair(it.first.removeSuffix("?").removeSuffix(",").removeSuffix("."), it.second) } //Removes suffixes
        //.map { Pair(it.first.lowercase(), it.second) } //Makes lowercase
        .filter { !it.first.first().isUpperCase() }
        .filter { !it.first.contains(Regex("[^A-Za-z0-9]")) } //Filters out all non-latin characthers

            //FIXME: END OF WORD CRITERIA
        .groupBy { it.first }.map { Pair(it.key, it.value.sumOf { it.second }) }.sortedByDescending { it.second }

    mostCommonWords = mostCommonWords.filter {
        it.first.length > 2
    }

    val wordCategories = mostCommonWords.groupBy { it.first.length }.toSortedMap()
    wordCategories.forEach {
        println("${it.key} letter words (${it.value.count { it.second >= 100 }}/${it.value.size})")
        it.value.filter { it.second >= 100 }.forEach {
            println("\t$it")
        }
    }
    */


/*
    popSentances = popSentances.take(40)
    println(popSentances)
    */

    /*


*/
/*
    val benchmark = measureTimeMillis {

        val sessionsWithParticipants = participants.map {
            // Pair(it, async(threadpool) {dataReader.getParticipantData(it.PARTICIPANT_ID)})
            Pair(it, dataReader.getParticipantData(it.PARTICIPANT_ID))
        }.toMap()
        val results = sessionsWithParticipants.values
        //println(results.size)
    }

    println("Without async: ${benchmark}ms")

    val withAsync = measureTimeMillis {




        //println(results)
    }

    println("With async: ${withAsync}ms")
*/
/*
    val x = dataReader.getParticipantData(participants.first().PARTICIPANT_ID)
    x.forEach {
        println("Session: ${it}")
        it.entries.forEach {
            println("\t$it")
        }
    }
    println(x)
*/
}

data class InterKeyDelay(
    val pID: Int,
    val a: String,
    val b: String,
    val delay: Long
)

val INVALID_TIMING = InterKeyDelay(-1, "ERROR", "ERROR", -1)

fun CreateInterKeyValue(word: Word): List<InterKeyDelay> {
    return arrayListOf()
}
