package dataset

data class Keystroke(
    val id: Int,
    val PRESS_TIME: Long,
    val RELEASE_TIME: Long,
    var INPUT: String,
    val KEYCODE: Int
)

data class TypeSession(
    val id: Int,
    val participant: Int,
    val sentance: Int,
    //val userInput: String //todo: might remove
) {
    lateinit var entries: List<Keystroke>
}

