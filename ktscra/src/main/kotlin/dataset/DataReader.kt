package dataset

import java.io.File
import java.util.zip.ZipFile

interface DataReader {
    suspend fun getParticipants(): List<Participant>
    suspend fun getParticipantData(id: Int): List<TypeSession>
}

const val METADATA_PATH = "metadata_participants.txt"

class ZippedDataReader(val path: String) : DataReader {
    val zipFile = ZipFile(File(path))

    private fun readLines(name: String): List<String> {
        val zipEntry = zipFile.getEntry("Keystrokes/files/$name")

        val entryStream = zipFile.getInputStream(zipEntry)
        val entryReader = entryStream.reader(charset = Charsets.UTF_8)

        return entryReader.readLines()
    }

    override suspend fun getParticipants() : List<Participant> {
        val metadata = readLines(METADATA_PATH)
        /*
        println(metadata.map { it.split("\t")[9] }.distinct())
        println(metadata.first())
        println(metadata[1].split("\t"))
        */
        val participants = metadata.subList(1, metadata.size).map { Participant(it.split("\t").iterator()) }
        return participants
    }

    fun getSentancesByParticipant(id: Int) : List<String> {
        val allSessionEntries = readLines("${id}_keystrokes.txt").map { it.split("\t") }
        val sentances = allSessionEntries.filter { it.size == 9 }.map { it[2] }.distinct().drop(1)
        return sentances
    }

    override suspend fun getParticipantData(id: Int) : List<TypeSession> {
        try {

        val allSessionEntries = readLines("${id}_keystrokes.txt").iterator()
        val typingSessions = mutableListOf<TypeSession>()
        //println(allSessionEntries.next()) //Skip instead
        allSessionEntries.next() //Skip header
        var currentLineRaw = allSessionEntries.next()
        var currentLine = currentLineRaw.split("\t")
        var sentanceID = ALL_SENTANCES.indexOf(currentLine[2])
        //var enteredByUser = currentLine[3]
        var sessionID = currentLine[1].toInt()

        var currentSession = TypeSession(sessionID, id, sentanceID/*, enteredByUser*/)
        var keystrokes = mutableListOf<Keystroke>()
        keystrokes.add(Keystroke(currentLine[4].toInt(),
            currentLine[5].toLong(),
            currentLine[6].toLong(),
            currentLine[7].ifBlank { "<TAB>" },
            currentLine[8].toInt()
        ))
        do {
            currentLineRaw = allSessionEntries.next()
            currentLine = currentLineRaw.replace("\t\t\t", "\t<TAB>\t").split("\t")
            //try {

            sessionID = currentLine[1].toInt()
            if (sessionID != currentSession.id) {
                currentSession.entries = keystrokes
                typingSessions.add(currentSession) //Add last session

                sentanceID = ALL_SENTANCES.indexOf(currentLine[2])
                //enteredByUser = currentLine[3]
                currentSession = TypeSession(sessionID, id, sentanceID/*, enteredByUser*/)
                keystrokes = mutableListOf()
            }
            /*
            } catch(e: Exception) {
                println("Block1: $e")
                println(currentLine)
                println(currentLineRaw)
                println(allSessionEntries.next())
                println(allSessionEntries.next())
                println(allSessionEntries.next())
                println(id)
            }
*/


               // val keyCode = currentLine[8].toInt()

            keystrokes.add(Keystroke(currentLine[4].toInt(),
                currentLine[5].toLongOrNull()?: -1,
                currentLine[6].toLongOrNull()?: -1,
                currentLine[7]/*.ifBlank { when(keyCode) {
                        32 -> "<SPACE>"
                    else -> {
                     //   println("UNKNOWN KEYCODE $keyCode")
                        "<UNKNOWN>"
                    }
                } }*/,

                currentLine[8].toInt()
                ))

            //todo: if letter is empty (i think cus then it was thab), make it "TAB"
        } while (allSessionEntries.hasNext())
        currentSession.entries = keystrokes
        typingSessions.add(currentSession)
        /*
        var currentSession = TypeSession()
        println(allSessionEntries[0])
        allSessionEntries.forEach {
        //    println(it)
        }
         */

        return typingSessions
        } catch (e: Exception) {
            println("Participant $id, had invalid type session data, invalidating all their sessions, error: $e")
            return emptyList()
        }

    }
}