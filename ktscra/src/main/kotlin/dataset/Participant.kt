package dataset

enum class Gender {
	none,
	male,
	female
}

enum class KBLayout {
	qwerty,
	azerty,
	qwertz,
	dvorak
}

enum class KeyboardType {
	full,
	laptop,
	`on-screen`,
	small
}

enum class TypeStyle(val personal_comments: String) {
	`1-2`("Amature"),
	`3-4`("Beginner"),
	`5-6`("Medium"),
	`7-8`("Proficent"),
	`9-10`("Good"),
	`10+`("How?");

	companion object {
		val Amature = `1-2`
		val Beginner = `3-4`
		val Medium = `5-6`
		val Proficent = `7-8`
		val Good = `9-10`
	}
}



class Participant (stringified: Iterator<String>) {
    val PARTICIPANT_ID= stringified.next().toInt();
	val AGE= stringified.next().toInt();
	val GENDER= stringified.next().toEnum<Gender>();
	val HAS_TAKEN_TYPING_COURSE= stringified.next().toInt() == 1 //0 or 1, is a bool
	val COUNTRY= stringified.next().toString(); //Country code, might turn into enum
	val LAYOUT= stringified.next().toEnum<KBLayout>();
	val NATIVE_LANGUAGE= stringified.next(); //i.e en
	val FINGERS= stringified.next().toEnum<TypeStyle>();
	val TIME_SPENT_TYPING= stringified.next().toInt()
	val KEYBOARD_TYPE= stringified.next().toEnum<KeyboardType>()
	val ERROR_RATE= stringified.next().toFloat()
	val AVG_WPM_15= stringified.next().toFloat()
	val AVG_IKI= stringified.next().toFloat();
	val ECPC= stringified.next().toFloat()
	val KSPC= stringified.next().toFloat();
	val ROR = stringified.next().toFloat();


}

private inline fun <reified T:Enum<T>> String.toEnum(): T = enumValueOf<T>(this)
